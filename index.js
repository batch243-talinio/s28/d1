// CRUD Operations
	/*
		- CRUD operations are the heart heart of any backend application.
		- mastering CRUD operations is essential for any developers.
		- this helps in building character and increasing exposure to logical statements that will help us manipulate.

	*/

// [Section] Inserting Document (Create)
	
	// Insert one Document
		/*
			- since MongoDB deals w/ objects as it's structure for documents, we can easily create them by providing objects into our methods.
			- Mongo shells also uses JS for its syntax w/c makes it convenient for us to understand it's code.

			syntax:
				db.collectionName.insertOnce({object});

			JavaScript:
				object.object.method({object});
		*/

	db.users.insert({
		firstName: "Jane",
		lastName: "Doe",
		age: 22,
		contct: {
			phone: "12345786",
			email: "janeDoe@isekai.com"
		},
		courses: ['CSS', 'JavaScript', 'Python'],
		department: "none"
	});


	// Insert Many
		/*
			syntax:
				db.collectionName.insertMany([{ObjectA}, {ObjectB}])
		*/

	db.users.insertMany([{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contct: {
				phone: "654654678",
				email: "stephenHawking@isekai.com"
			},
			courses: ['Python', 'React', 'PHP'],
			department: "none"
		}, {
			firstName: "Niel",
			lastName: "Armstrong",
			age: 82,
			contct{
				phone: "42141243",
				email: "neilarmstrong@isekai.com"
			},
			courses: ['React', 'Laravel', 'SASS'],
			department: "none"
		}
	])

// [Section] Finding Documents (read)

	// Find
	/*
		- if multiple documents match the criteria for finding a document, only the first document that matches the search term will be returned.
		- this is based from order that the documents are stored in collection.

		syntax:
			db.collectionName.find({field: value});
	*/

	// leaving the saerch criteria empty will retrieve all documents.
	db.users.find();

	db.users.find({firstName: "Stephen"});

	// pretty method
		// - pretty method allows us to be able to view the documents returned by our terminals (e.g. gitBash) in a better format.

		db.users.find({firstName: "Stephen"}).pretty();


	// finding documents w/ multiple parameters

		/*
			syntax:
				db.collectionName.find({fieldA: valueA, fieldB: valueB});
		*/

		db.users.find({lastName: "Armstrong", age: 82}).pretty();


// [Section] Updating Documents

	// updating a single document

		db.users.insert({
			firstName: "Test",
			lastName: "TestTest",
			age: 0,
			contct: {
				phone: "000000",
				email: "test@isekai.com"
			},
			courses: [],
			department: "none"
		})

		/*
			- just like the find method, updateOne method will only manipulate a single document. First document that matches the search criteria will be updated.

			syntax:

				db.collectionName.updateOne({criteria}, {$set: {field:value }})
		*/

		db.users.updateOne(
			{firstName: "Bill"},
			{
				#set:{
					lastName: "Bill",
					age: 65,
					contct: {
						phone: "12345678",
						email: "bill@windows.com"
					},
					courses: ["PHP", "Laravel", "HTML"],
					department: "Operations",
					status: "active"
				}
			}
		);
	

	// Updating multiple documents
		 /*
			db.collectionName.updateMany({criteria}, {$set: {field:value }})
		 */

		db.users.updateMany({department: "none"}, {
			$set: {
				department: "HR"
			}
		})


	// Replace One
		// used in replacing whole document when necessary

		/*
			syntax:
				db.collectionName.replaceOne({criteria},{object}});
		*/
		// _id can also be used
		// _id: ObjectId("637f1024fc3ddbf574a0c333") 

		db.users.replaceOne({firstName: "Bill"},
			{
				firstName: "Rudeus",
				lastName: "Greyrat",
				age: 15,
				contct: {
					phone: "231693213",
					email: "rudeusNG@isekai.com"
				},
				courses: ["P4NtzU", "Boviees"],
				department: "Echi"
			}
		)


	// Deleting a single document

		/*
			syntax:
				db.collectionName.deleteOne({criteria});
		*/

		db.users.deleteOne({firstName: "test"});

	// Delete Many

		/*
			- be careful when using the deleteMany method. if no search criteria is provided it will delete all documents in the collection.
			- NOT RIGHT WAY TO USE!
				db.collectionName.deleteMany({});

			- RIGHT WAY TO USE!
				db.collectionName.deleteMany({criteria});
		*/

		db.users.deleteMany({firstName: "Niel"});

// [Section] Advance queries
	// Query an embedded document
		db.users.find({
			contct: {
				phone: "12345786",
				email: "janeDoe@isekai.com"
			}
		})

	// query on nested field
		db.users.find({"emails": ["rudeusNG@isekai.com","bill@windows.com"]})
//try	// "emails": ["test@gmail.com","test2@gmail.com"]


	// Querying an array with exact elements
		db.users.find({courses: ["Python", "React", "PHP"]});

	// Querying an array w/o regard to order and elements.
		db.users.find({courses: {$all : ["React", "Laravel"]}});